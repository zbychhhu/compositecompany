package employees;

public interface Manager extends Employee {

    void hire(Employee e) throws Exception;

    void fire(Employee e) throws Exception;

    boolean canHire();
}
