package employees;

import report.Report;
import additional.Role;
import additional.Task;

public abstract class AbstractEmployee implements Employee {

    private Role role;
    private final String name;
    private final String surname;


    public AbstractEmployee(String name, Role role,String surname) {
        this.role = role;
        this.name = name;
        this.surname=surname;

    }

    @Override
    public abstract void assign(Task task);

    @Override
    public abstract Report reportWork();

    @Override
    public abstract Task getTask();

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSurname() {
        return surname;
    }

    @Override
    public Role getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "Role: " + role + " Name: " + name + " Surname: " + surname;
    }
}
