package employees;

import additional.*;
import report.Report;
import report.TeamManagerReport;

import java.util.ArrayList;
import java.util.List;

public class TeamManager extends AbstractEmployee implements Manager {


    Report report;
    private List<Employee> listEmployees;
    private final int max = 10;

    public TeamManager(Role role, String name,String surname) {
        super(name, role,surname);
        listEmployees = new ArrayList<>(max);
        report = new TeamManagerReport();
    }


    @Override
    public Report reportWork() {

        return report;
    }

    @Override
    public Task getTask() {
        return null;
    }

    @Override
    public void assign(Task task) {

        listEmployees.forEach(employee -> {
            if (employee.getTask() == null) {
                employee.assign(task);
                report.saveToRaport(task);
            }
        });


    }

    @Override
    public void hire(Employee employee) throws Exception {

        if (canHire()) {
            listEmployees.add(employee);
            if (employee.getTask() != null) {
                report.saveToRaport(employee.getTask());
            }
            ;
        } else throw new Exception("TeamManager can't hire worker");

    }

    @Override
    public void fire(Employee e) throws Exception {
        if (listEmployees.size() == 0)
            listEmployees.remove(e);
        else throw new Exception("TeamManager doesn't have a workers");
    }

    @Override
    public boolean canHire() {
        return listEmployees.size() < max;
    }

    @Override
    public String toString() {

        return super.toString() + " Team: \t\n\t" + listEmployees.toString() + "\t";
    }
}
