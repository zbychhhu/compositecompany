package employees;

import report.DeveloperReport;
import report.Report;
import additional.Role;
import additional.Task;

public class Developer extends AbstractEmployee {

    private DeveloperReport report;
    private Task task;

    public Developer(String name,String surname) {

        super(name, Role.Developer,surname);
        report = new DeveloperReport();
    }


    @Override
    public Task getTask() {
        return task;
    }

    @Override
    public void assign(Task task) {

        this.task = task;
        report.saveToRaport(task);
    }

    @Override
    public Report reportWork() {

        return report;

    }

    @Override
    public String toString() {
        return super.toString() + " Task: " + task.getTask() + " " + reportWork();
    }
}
