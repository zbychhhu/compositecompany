package employees;

import report.Report;
import additional.Role;
import additional.Task;

public interface Employee {

    String getName();

    String getSurname();

    Role getRole();

    Task getTask();

    Report reportWork();

    void assign(Task task);

}
