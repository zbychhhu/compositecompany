package random;

import additional.Role;
import additional.Task;
import employees.Developer;
import employees.Employee;
import employees.Manager;
import employees.TeamManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.List;
import java.util.stream.IntStream;

public class RandomStructure {

    private int size;
    private Data data;
    private List<Employee> company;
    private List list;
    private Random random;
    private String[] tasks;

    public RandomStructure(int size, List<String> listNames,List<String> listSurnames) throws Exception {

        data = new Data(listNames,listSurnames);
        this.size = size;
        company = new ArrayList<>(10);
        list = new ArrayList(10);
        random = new Random();
        tasks = new String[]{"Implement new function", "Fix bugs", "Refactor", "Get to know new technology"};

        randomMachine();

    }

    private String randomSurname() throws IOException{
        return RandomElement(data.getListSurnames());
    }

    private String randomName() throws IOException {

        return RandomElement(data.getListNames());

    }

    private String RandomElement(List<String> list) {

        return list.get(random.nextInt(list.size()));
    }

    public Developer randomDeveloper() throws IOException {
        return new Developer(randomName(),randomSurname());
    }

    public TeamManager randomDevelopmentManager() throws IOException {
        return new TeamManager(Role.Development_manager, randomName(),randomSurname());
    }


    private Task randomTask() {
        return new Task(tasks[random.nextInt(tasks.length)], random.nextInt(50) + 1);
    }

    private void randomCompany() {
        company.add(new TeamManager(Role.CEO, "Boss","Kowalski"));
        IntStream.range(1, size).forEach(i -> {

            if (i + 1 < size) {
                IntStream.range(0, (int) Math.pow(2, i)).forEach(j -> {
                    try {
                        company.add(randomDevelopmentManager());
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                });
            } else {
                IntStream.range(0, (int) Math.pow(2, i)).forEach(j -> {
                    try {
                        company.add(randomDeveloper());
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                });
            }

        });
    }


    private void randomHire() {

        int j = 0;
        for (int i = 1; i < company.size(); i++) {
            try {
                ((TeamManager) company.get(j)).hire(company.get(i));
            } catch (Exception e) {
                System.out.println(e);
            }
            if (i % 2 == 0)
                j++;

        }


    }

    private void randomAssign() {
        for (Employee e : company) {
            e.assign(randomTask());

        }
    }

    private void randomMachine() throws Exception {
        randomCompany();
        randomHire();
        randomAssign();
    }

    @Override
    public String toString() {

        return company.toString();
    }


}
