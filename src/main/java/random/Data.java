package random;

import com.google.common.collect.ImmutableList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class Data {
    private List<String> listNames;
    private List<String> listSurnames;

    private final String FilePathNames = "/src/main/resources/names.txt";
    private final String FilePathSurnames = "/src/main/resources/surnames.txt";

    Data(List<String> listNames,List<String> listSurnames) {
        this.listNames = listNames;
        this.listSurnames = listSurnames;

    }

    public List<String> getListNames() throws IOException {
        if (listNames.size() == 0)
            readFile(listNames, FilePathNames);

        return ImmutableList.copyOf(listNames);
    }

    public List<String> getListSurnames() throws IOException {
        if (listSurnames.size() == 0)
            readFile(listSurnames, FilePathSurnames);

        return ImmutableList.copyOf(listSurnames);
    }


    private void readFile(List<String> list, String filePath) throws IOException {
        String workingDirectory = System.getProperty("user.dir");
        String absoluteFilePath = workingDirectory + File.separator + filePath;
        FileReader fileReader = new FileReader(absoluteFilePath);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        list.clear();

        try {
            String textLine = bufferedReader.readLine();
            do {
                list.add(textLine);

                textLine = bufferedReader.readLine();
            } while (textLine != null);
        } finally {
            bufferedReader.close();
        }
    }
}
