package report;

import additional.Task;

public abstract class AbstractReport implements Report {
    private int sum;

    public AbstractReport() {
        sum = 0;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public abstract void saveToRaport(Task task);

    @Override
    public String toString() {
        return String.valueOf(getSum());
    }
}
