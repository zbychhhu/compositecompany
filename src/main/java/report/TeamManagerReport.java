package report;


import additional.Task;

public class TeamManagerReport extends AbstractReport {

    public TeamManagerReport() {
        super();
    }

    @Override
    public void saveToRaport(Task task) {
        setSum(getSum() + task.getUnitsOfWork());
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
