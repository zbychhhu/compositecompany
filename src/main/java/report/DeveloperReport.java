package report;

import additional.Task;

public class DeveloperReport extends AbstractReport {


    public DeveloperReport() {
        super();
    }

    @Override
    public void saveToRaport(Task task) {
        setSum(task.getUnitsOfWork());
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
